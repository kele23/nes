#include "tasks/SenderTask.hh"

SenderTask::SenderTask( const sc_core::sc_module_name modulename,
          const task_id_t id,
          Scnsl::Core::Node_t * n,
          const size_t proxies,
          ReceiverTask* receiver_task) : Scnsl::Tlm::TlmTask_if_t(modulename,id,n,proxies),_receiver_task(receiver_task),
            currBits(0),timeout(1e12),sPackets(0),speed(8){

    SC_THREAD( _sender );
    SC_THREAD( _send );
}

SenderTask::~SenderTask(){

}

void SenderTask::b_transport(tlm::tlm_generic_payload & p, sc_core::sc_time & t ){}

void SenderTask::Print(){
    static int64_t old_received_bits = 0;

    std::cout   << std::setw(11) << std::setfill('0') << sc_core::sc_time_stamp().to_double() << "\t"
                << speed << "\t"
                << currBits << "\t"
                << std::setw(8) << std::setfill('0') << _receiver_task->getReceivedPackets()*8 - old_received_bits << std::endl;

    old_received_bits = _receiver_task->getReceivedPackets()*8;
}

void SenderTask::_send(){

    const Scnsl::Tlm::TlmTask_if_t::size_t size = 1;
    const std::string taskproxy = "0";
    byte_t i = byte_t( 1 );

    while(true){
        wait(_send_event);
        TlmTask_if_t::send(taskproxy, &i, size); //send packet
    }

}

void SenderTask::_sender(){

    const Scnsl::Tlm::TlmTask_if_t::size_t size = 1;
    const std::string taskproxy = "0";
    byte_t i = byte_t( 1 );

    long maxPackets = 2000000;
    static sc_core::sc_time old = sc_core::sc_time_stamp();

    double sd_time = 0;

    while(sPackets < maxPackets){

        sc_core::sc_time now = sc_core::sc_time_stamp();

        if( (now - old ) >= sc_core::sc_time(1,sc_core::SC_SEC) ){
            Print();

            if( speed < (1 << 21)){
                timeout = timeout >> 1;
                speed = speed << 1;
            }
            old = now;
            currBits = 0;
        }

        double bf_send = sc_core::sc_time_stamp().to_double();
        //TlmTask_if_t::send(taskproxy, &i, size); //send packet
        _send_event.notify();
        double af_send = sc_core::sc_time_stamp().to_double();

        sd_time+= (af_send - bf_send);

        currBits+=8;
        sPackets++;
        if(sPackets < maxPackets){
            wait( timeout , sc_core::SC_PS);
        }
    }

    Print();
    std::cout << "Send function Time: " << sd_time/maxPackets << std::endl;

}
