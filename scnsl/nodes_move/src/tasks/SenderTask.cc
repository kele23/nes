#include "tasks/SenderTask.hh"

SenderTask::SenderTask( const sc_core::sc_module_name modulename,
          const task_id_t id,
          Scnsl::Core::Node_t * n,
          const size_t proxies,
          Scnsl::Core::Channel_if_t* channel,
          int sensor_ID,
          ReceiverTask* receiver ) : Scnsl::Tlm::TlmTask_if_t(modulename,id,n,proxies),
          _channel(channel), _sensor_ID(sensor_ID),_receiver_task(receiver){

    SC_THREAD( _sender );
    SC_THREAD( _mover );
}

SenderTask::~SenderTask(){

}

void SenderTask::b_transport(tlm::tlm_generic_payload & p, sc_core::sc_time & t ){

}

void SenderTask::_sender(){

    while(true){

        Payload payload;
        const Scnsl::Tlm::TlmTask_if_t::size_t size = static_cast<Scnsl::Tlm::TlmTask_if_t::size_t>(sizeof(payload));
        const std::string taskproxy = "0";
        payload.sender_id = static_cast<uint8_t>(_sensor_ID);

        sc_core::sc_time old = sc_core::sc_time_stamp();

        TlmTask_if_t::send(taskproxy, reinterpret_cast<Scnsl::Core::byte_t*>(&payload), size);

        sc_core::sc_time now = sc_core::sc_time_stamp();

        /*std::string status = _receiver_task->isReceived(_sensor_ID) ? "SUCCESS" : "ERROR";*/
        std::string status = "UNKNOWN";

        Scnsl::Core::node_properties_t properties = this->getNode()->getProperties(_channel);
        std::cout << _sensor_ID << "\t" << status << "\t" << ( old.value() / 1e9 ) << "\t" << ((now-old).value() / 1e6) << "\t" << properties.x << "\t" << properties.y << std::endl;

        wait(_sensor_ID, sc_core::SC_SEC);
    }

}

void SenderTask::_mover(){

    float theta = 0;
    float omega = 1.0f / _sensor_ID; //velocità angolare

    Scnsl::Core::Node_t* node = this->getNode();
    Scnsl::Core::node_properties_t properties = node->getProperties(_channel);

    while(true){
        wait(10, sc_core::SC_MS);

        theta += omega * 1e-2;

        properties.x = cosf(theta)*_sensor_ID;
        properties.y = sinf(theta)*_sensor_ID;
        properties.z = 0;

        node->setProperties(properties,_channel,false);
    }

}
