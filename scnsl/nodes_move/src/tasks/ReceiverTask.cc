#include "tasks/ReceiverTask.hh"

ReceiverTask::ReceiverTask( const sc_core::sc_module_name modulename,
          const task_id_t id,
          Scnsl::Core::Node_t * n,
          const size_t proxies) : Scnsl::Tlm::TlmTask_if_t(modulename,id,n,proxies){

    //SC_THREAD(clear);
}

ReceiverTask::~ReceiverTask(){

}

void ReceiverTask::b_transport(tlm::tlm_generic_payload &p, sc_core::sc_time &time){
    if(p.get_command() == Scnsl::Tlm::PACKET_COMMAND){
        Payload*  payload = reinterpret_cast<Payload*>(p.get_data_ptr());

        sc_core::sc_time now = sc_core::sc_time_stamp();

        //recMap[payload->sender_id] = true;
    }
}

void ReceiverTask::clear(){
    wait(500,sc_core::SC_MS);
    recMap.clear();
}

bool ReceiverTask::isReceived(uint8_t sensor_ID){
    return recMap[sensor_ID];
}
