#include <systemc>
#include <tlm>
#include <scnsl.hh>
#include <iostream>
#include "tasks/ReceiverTask.hh"
#include "tasks/SenderTask.hh"


using namespace Scnsl::Setup;
using namespace Scnsl::BuiltinPlugin;
using Scnsl::Tracing::Traceable_base_t;

int main(int argc, char* argv[]){

	try {

		std::cout << "Source\tStatus\tSend\tDelay\tX\tY" << std::endl;

		int N = 30;

        // Singleton.
        Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

        CoreChannelSetup_t ccs;

        ccs.extensionId = "core";
        ccs.channel_type = CoreChannelSetup_t::SHARED;
        ccs.name = "SharedChannel";
        ccs.alpha = 0.1;
		//ccs.capacity = 1000;
        ccs.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs.nodes_number = N+1;

        Scnsl::Core::Channel_if_t * ch = scnsl->createChannel( ccs );
        Scnsl::Utils::DefaultEnvironment_t::createInstance(ccs.alpha);

		const Scnsl::Core::size_t PROXIES = 1;

		// Nodes creation:
		// Receiver
		Scnsl::Core::Node_t * receiver_node = scnsl->createNode();
        const Scnsl::Core::task_id_t receiver_id = 0;
        ReceiverTask* receiver_task = new ReceiverTask( "Receiver", receiver_id, receiver_node, PROXIES );

		// Sender
		std::vector<Scnsl::Core::Node_t*> senderNodes;
		std::vector<SenderTask*> senderTasks;
		for(int i = 0; i < N; i++){
			Scnsl::Core::Node_t * sender_node = scnsl->createNode();
	        const Scnsl::Core::task_id_t sender_id = i+1;
			std::string name = "Sender"+std::to_string(i+1);
	        SenderTask* sender_task = new SenderTask( name.c_str(), sender_id, sender_node, PROXIES, ch , static_cast<float>(i+1), receiver_task);

			senderNodes.push_back(sender_node);
			senderTasks.push_back(sender_task);
		}

        // Creating the protocol 802.15.4:
        CoreCommunicatorSetup_t ccoms;
        ccoms.extensionId = "core";
        ccoms.ack_required = true; /// <- Importante
        ccoms.short_addresses = true;
        ccoms.type = CoreCommunicatorSetup_t::MAC_802_15_4;

		// Receiver
        ccoms.name = "ReceiverMac";
        ccoms.node = receiver_node;
        Scnsl::Core::Communicator_if_t * receiver_mac = scnsl->createCommunicator( ccoms );

		std::vector<Scnsl::Core::Communicator_if_t *> senderMacs;
		for(int i = 0; i < N; i++){
			ccoms.name = "SenderMac" + std::to_string(i+1);
	        ccoms.node = senderNodes[i];
	        Scnsl::Core::Communicator_if_t * sender_mac = scnsl->createCommunicator( ccoms );

			senderMacs.push_back(sender_mac);
		}

		// Binding
        BindSetup_base_t receiver_bsb;
        receiver_bsb.extensionId = "core";
        receiver_bsb.destinationNode = NULL;
        receiver_bsb.node_binding.x = 0;
        receiver_bsb.node_binding.y = 0;
        receiver_bsb.node_binding.z = 0;
        receiver_bsb.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        receiver_bsb.node_binding.transmission_power = 100;
        receiver_bsb.node_binding.receiving_threshold = 10;

        scnsl->bind( receiver_node, ch, receiver_bsb );
        scnsl->bind( receiver_task, NULL, ch, receiver_bsb, receiver_mac );

		for(int i = 0; i < N; i++){
	        BindSetup_base_t bsb1;
	        bsb1.extensionId = "core";
	        bsb1.destinationNode = receiver_node; // don't care: it receives only...
	        bsb1.node_binding.x = (i+1);
	        bsb1.node_binding.y = 0;
	        bsb1.node_binding.z = 0;
	        bsb1.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
	        bsb1.node_binding.transmission_power = 100;
	        bsb1.node_binding.receiving_threshold = 10;

	        scnsl->bind( senderNodes[i], ch, bsb1 );
	        scnsl->bind( senderTasks[i], receiver_task, ch, bsb1, senderMacs[i] );
		}

        sc_core::sc_start( sc_core::sc_time( 90, sc_core::SC_SEC ) );

	}
    catch ( std::exception & e)
    {
        std::cerr << "Exception in sc_main(): " << e.what() << std::endl;
        return 1;
	}

	return 0;
}
