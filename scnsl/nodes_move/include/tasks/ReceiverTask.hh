#ifndef _RECEIVER_TASK_HH_
#define _RECEIVER_TASK_HH_

#include <systemc>
#include <scnsl.hh>
#include <string>
#include <cstring>
#include <tasks/Payload.hh>
#include <iomanip>

class ReceiverTask : public Scnsl::Tlm::TlmTask_if_t {

  public:
    SC_HAS_PROCESS( ReceiverTask );

    ReceiverTask( const sc_core::sc_module_name modulename,
              const task_id_t id,
              Scnsl::Core::Node_t * n,
              const size_t proxies);

    /// @brief Virtual destructor.
    virtual ~ReceiverTask();

    virtual void b_transport(tlm::tlm_generic_payload & payload, sc_core::sc_time & time );

    bool isReceived(uint8_t sensor_ID);

  private:

    std::map<uint8_t,bool> recMap;

    void clear();

    ReceiverTask( const ReceiverTask & );
    ReceiverTask & operator = ( ReceiverTask & );

};


#endif
