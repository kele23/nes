#ifndef _SENDER_TASK_HH_
#define _SENDER_TASK_HH_

#include <systemc>
#include <scnsl.hh>
#include <string>
#include <cstring>
#include <systemc>
#include <scnsl.hh>
#include <string>
#include <cstring>
#include <tasks/Payload.hh>
#include <iomanip>
#include "tasks/ReceiverTask.hh"
#include "tasks/Payload.hh"

class SenderTask : public Scnsl::Tlm::TlmTask_if_t {

  public:
    SC_HAS_PROCESS( SenderTask );

    SenderTask( const sc_core::sc_module_name modulename,
              const task_id_t id,
              Scnsl::Core::Node_t * n,
              const size_t proxies,
              Scnsl::Core::Channel_if_t* channel,
              int sender_ID,
              ReceiverTask* receiver_task);

    /// @brief Virtual destructor.
    virtual ~SenderTask();

    void b_transport(tlm::tlm_generic_payload & p, sc_core::sc_time & t );

  private:

    /// @brief Sender process.
    void _sender();
    void _mover();

    /// @brief The channel
    Scnsl::Core::Channel_if_t* _channel;
    int _sensor_ID;
    ReceiverTask* _receiver_task;

    SenderTask( const SenderTask & );
    SenderTask& operator = ( SenderTask & );

};


#endif
