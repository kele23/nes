#ifndef _SENDER_TASK_HH_
#define _SENDER_TASK_HH_

#include <systemc>
#include <scnsl.hh>
#include <string>
#include <cstring>
#include <iostream>
#include <iomanip>
#include "ReceiverTask.hh"

class SenderTask : public Scnsl::Tlm::TlmTask_if_t {

  public:
    SC_HAS_PROCESS( SenderTask );

    SenderTask( const sc_core::sc_module_name modulename,
              const task_id_t id,
              Scnsl::Core::Node_t * n,
              const size_t proxies,
              ReceiverTask* receiver_task);

    /// @brief Virtual destructor.
    virtual ~SenderTask();

    virtual void b_transport(tlm::tlm_generic_payload & p, sc_core::sc_time & t );

  private:

    ReceiverTask* _receiver_task;
    long sPackets;
    long timeout;
    long speed;
    long currBits;

    /// @brief Sender process.
    void _sender();
    void Print();

    SenderTask( const SenderTask & );
    SenderTask& operator = ( SenderTask & );

};


#endif
