#ifndef _RECEIVER_TASK_HH_
#define _RECEIVER_TASK_HH_

#include <systemc>
#include <scnsl.hh>
#include <string>
#include <cstring>

class ReceiverTask : public Scnsl::Tlm::TlmTask_if_t {

  public:
    ReceiverTask( const sc_core::sc_module_name modulename,
              const task_id_t id,
              Scnsl::Core::Node_t * n,
              const size_t proxies);

    /// @brief Virtual destructor.
    virtual ~ReceiverTask();

    void b_transport(tlm::tlm_generic_payload & p, sc_core::sc_time & t );

    long getReceivedPackets();

  private:

    long _receivedPackets;

    ReceiverTask( const ReceiverTask & );
    ReceiverTask& operator = ( ReceiverTask & );

};


#endif
