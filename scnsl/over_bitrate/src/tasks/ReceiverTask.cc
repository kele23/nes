#include "tasks/ReceiverTask.hh"

ReceiverTask::ReceiverTask( const sc_core::sc_module_name modulename,
          const task_id_t id,
          Scnsl::Core::Node_t * n,
          const size_t proxies) : Scnsl::Tlm::TlmTask_if_t(modulename,id,n,proxies) , _receivedPackets(0){}

ReceiverTask::~ReceiverTask(){

}

void ReceiverTask::b_transport(tlm::tlm_generic_payload & p, sc_core::sc_time & t ){
    if(p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND))
	{
        //std::cout << "Rec" << std::endl;
		_receivedPackets += p.get_data_length();
	}
}

long ReceiverTask::getReceivedPackets(){
    return _receivedPackets;
}
