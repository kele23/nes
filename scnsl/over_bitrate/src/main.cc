#include <systemc>
#include <tlm>
#include <scnsl.hh>
#include <iostream>
#include "tasks/SenderTask.hh"
#include "tasks/ReceiverTask.hh"


using namespace Scnsl::Setup;
using namespace Scnsl::BuiltinPlugin;
using Scnsl::Tracing::Traceable_base_t;

int main(int argc, char* argv[]){

	try {

        // Singleton.
        Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

		// Channel
        CoreChannelSetup_t ccs;
        ccs.extensionId = "core";
        ccs.channel_type = CoreChannelSetup_t::UNIDIRECTIONAL;
        ccs.name = "UnidirectionalChannel";
		ccs.capacity = 1e6; //1 Mega Bits
        ccs.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        Scnsl::Core::Channel_if_t * ch = scnsl->createChannel( ccs );

		const Scnsl::Core::size_t PROXIES = 1;

		// Nodes creation:
		// Receiver
		Scnsl::Core::Node_t * receiver_node = scnsl->createNode();
        const Scnsl::Core::task_id_t receiver_id = 0;
        ReceiverTask* receiver_task = new ReceiverTask( "Receiver", receiver_id, receiver_node, PROXIES );

		// Sender
		Scnsl::Core::Node_t * sender_node = scnsl->createNode();
        const Scnsl::Core::task_id_t sender_id = 1;
        SenderTask* sender_task = new SenderTask( "Sender", sender_id, sender_node, PROXIES, receiver_task);

		// Binding
		BindSetup_base_t receiver_bsb;
        BindSetup_base_t sender_bsb;
        receiver_bsb.extensionId = "core";
        sender_bsb.extensionId = "core";

        scnsl->bind( sender_node, ch, sender_bsb );
		scnsl->bind( receiver_node, ch, receiver_bsb );
        scnsl->bind( sender_task, NULL, ch, sender_bsb, NULL );
		scnsl->bind( receiver_task, NULL, ch, receiver_bsb, NULL );

		std::cout   << "Time" << "\t"
	                << "Ideal" << "\t"
	                << "Current" << "\t"
	                << "Received" << std::endl;

        sc_core::sc_start( sc_core::sc_time( 40, sc_core::SC_SEC ) );

	}
    catch ( std::exception & e)
    {
        std::cerr << "Exception in sc_main(): " << e.what() << std::endl;
        return 1;
	}

	return 0;
}
