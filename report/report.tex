\documentclass{article}
\usepackage{graphicx}
\usepackage{color}
\usepackage[utf8]{inputenc}

\usepackage{listings}

\lstset{language=C++,
    basicstyle=\ttfamily,
    keywordstyle=\color{blue}\ttfamily,
    stringstyle=\color{red}\ttfamily,
    commentstyle=\color{green}\ttfamily,
    morecomment=[l][\color{magenta}]{\#}
}

\begin{document}

    \title{Progetto NES}
    \author{Michele Scala}

    \maketitle

    \section{Obiettivo}
    Il progetto consiste nell'analisi e nel confronto dei simulatori di rete,
    NS-3 e SCNSL, prendendo come riferimento lo studio di due classici schemi:
    \begin{itemize}
        \item Point-to-Point channel saturation.
        \item 802.15.4 Environment monitoring system.
    \end{itemize}
    Il confronto viene eseguito su di un laptop con processore Intel Core i5-4200u,
    considerando, per le tempistiche, i campi \textit{user+sys} riportati
    dal comando linux \textit{time}.


    \subsection{SCNSL}
    SCNSL è una libreria gratuita che estende SystemC, un potente sistema di
    simulazione e sviluppo di sistemi hardware. Sviluppato da EDAlab si prefigge
    come obiettivo quello di permettere una modellazione di
    rete basate su pacchetti, principalmente per sistemi embedded.

    \subsection{NS-3}
    NS-3 è un progetto di un simulatore ad eventi discreti per simulazione di
    rete nato nel 2006, sviluppato da un team di esperti e sostenuto dalla U.S.
    National Science Foundation (NSF), il quale obiettivo è la creazione di un
    ambiente Open-Source per la ricerca nel campo della comunicazione di rete.

    \pagebreak

    \section{Point-to-Point Channel Saturation}
    L'obiettivo di questo schema è di confrontare e valutare come i simulatori,
    si comportano quando una comunicazione punto-punto tra due nodi, chiamati
    ``Sender'' e ``Receiver'', supera la velocità massima di trasmissione del
    canale.
    In particolare si fà riferimento ad una situazione con i seguenti parametri:
    \begin{itemize}
            \item Canale Punto-Punto unidirezionale a 1 Mbps.
            \item Basso ritardo di propagazione: 1 $\mu$s.
            \item Pacchetti di dimensione 1 byte.
            \item Invio di 2'000'000 pacchetti (16'000'000 bit).
            \item Tempo massimo simulato: 40 secondi.
            \item Buffer di ricezione e invio: 1 byte.
            \item Raddoppio del velocità trasmissiva ogni secondo.
            \item Massima velocità di trasmissione del nodo: 2,097152 Mbps ($2^{21}$).
    \end{itemize}

    \begin{figure}[!ht]
        \centering
        \includegraphics[width=0.8\textwidth]{imgs/point-to-point-first2}
        \caption{Obiettivo del progetto}
    \end{figure}

    Ciò che ci interessa è capire se le
    simulazioni possono essere ritenute veritiere e se effettivamente si
    comportano come su di un canale reale, perdita di pacchetti e/o ritardi,
    confrontanto anche le prestazioni dei i due sistemi.

    \pagebreak

    \subsection{Risultati}

    \begin{figure}[!ht]
        \centering
        \includegraphics[width=0.8\textwidth]{imgs/scnsl_point_to_point}
        \caption{Grafico andamento SCNSL}
    \end{figure}

    \begin{figure}[!ht]
        \centering
        \includegraphics[width=0.8\textwidth]{imgs/ns3_point_to_point}
        \caption{Grafico andamento NS3}
    \end{figure}

    Entrambe le simulazioni restituiscono i seguenti parametri:
    \begin{itemize}
        \item \textbf{\textcolor{blue}{Ideal}}: Velocità ideale del sistema.
        \item \textbf{\textcolor{red}{Current}}: Velocità effettiva della
            funzione di invio dei dati.
        \item \textbf{\textcolor{yellow}{Received}}: Velocità ricezione dei dati.
    \end{itemize}

    Come si può vedere dai grafici sopra proposti, i due sistemi differiscono
    particolarmente tra di loro, sia nel tempo impiegato per eseguire la
    simulazione sia nel risultato della simulazione.

    \pagebreak

    \subsubsection{SCNSL}

    \begin{center}
        \begin{tabular}{l|l}
            Tempo simulazione & 1,824 sec \\
            Tempo simulato & 39,707 sec
        \end{tabular}
    \end{center}

    La simulazione della massima velocità trasmissiva di un canale viene
    implementata da SCNSL come un ritardo sulla funzione di invio, infatti, se
    si tenta di inviare più Bps di quanti il canale ne supporta,
    la Send viene bloccata e necessità di più tempo per essere eseguita.

    Ciò blocca l'esecuzione del Task che la usa, portando ad un problema
    fondamentale: Non si riesce a simulare un sistema che tenti di inviare ad
    una velocità maggiore di quanto il canale supporti.

    In effetti, come si può vedere nel grafico, l'effettiva velocità raggiunta
    dalla funzione di invio dei dati è di 677 Kbps e non 2,097 Mbps.
    Tutto ciò è spiegabile da come viene implementata la funzione di
    \textit{``Send''} all'interno dei canali SCNSL:

    \begin{verbatim}
_CAPACITY impostato a 1000000 (1 Mbps)

// Scheduling the packet.
wait( p.getSize() / _CAPACITY, sc_core::SC_SEC );\end{verbatim}

    Il codice sovrastante è contenuto nella funzione \textit{Send} all'interno
    di ogni canale, tranne mel modello \textit{``SharedChannel''}
    il quale ha un funzionamento leggermente diverso, esso implementa
    la limitazione di banda massima come un ritardo nella funzione di
    invio.

    Nell' oggetto di studio questo ritardo nell'invio si traduce nella
    situazione spiegata dalle seguenti formule:

    \begin{itemize}
        \item $T(send(1\; byte)) = 8\mu s$ Tempo impiegato dalla Send.
        \item $T(2,097\; Mbps) = 3,814\mu s$ Tempo di attensa tra due chiamate
            a Send, ogni send invia 8 bit.
    \end{itemize}
    Il simulatore somma i due tempi, attendendo
    un totale $11,814\mu s$, tra l'invio di due pacchetti da 8 bit,
    il che corrisponde ad una velocità trasmissiva di:
    \begin{center}$(1s\;/\;11,814\mu s)*8 = 677\; Kbps$.\end{center}
    In effetti proprio la velocità riscontrata nel grafico in output.

    Anche se il programmatore riducesse a $0$ il tempo di ritardo tra due
    chiamate a Send, ciò che otterrebbe sarebbe la capacità massima del canale,
    impostata ad inizio del codice, non si riuscirebbe lo stesso a simulare
    l'invio di più dati di quanto il canale supporti.

    Solamente modificando il codice e creando due processi separati, uno per
    l'invio dei dati, utilizzo della funzione \textit{Send}, e l'altro per la
    gestione si riesce a raggiungere lo scopo preposto di simulare un invio con
    perdita di informazioni causata da una velocità superiore a quella
    supportata dal canale.

    \begin{center}
        \begin{tabular}{l|l}
            Tempo simulazione & 1,045 sec \\
            Tempo simulato & 24,629 sec
        \end{tabular}
    \end{center}

    \begin{figure}[!ht]
        \centering
        \includegraphics[width=0.8\textwidth]{imgs/scnsl_point_to_point_v2}
        \caption{Grafico andamento SCNSL V2}
    \end{figure}

    In effetti l'output della simulazione è quello che ci si aspetta tranne che
    per la velocità di ricezione che non raggiunge la massima
    consentita per una problematica di gestione degli eventi di SystemC,
    un bug nel codice della simulazione, il
    quale non permette una piena sincronizzazione tra i due processi usati.
    Sicuramente con un diverso sistema di threads, o con la gestione di
    un'opportuna coda di pacchetti, si possono raggiungere gli obiettivi
    prefissati.

    \subsubsection{NS3}

    \begin{center}
        \begin{tabular}{l|l}
            Tempo simulazione & 18,817 sec \\
            Tempo simulato & 24,630 sec
        \end{tabular}
    \end{center}

    NS3 in questo caso sembra simulare più fedelmente una trasmissione reale,
    esso, infatti, supporta l'invio di più dati rispetto alla banda del canale,
    simulando la perdita di informazioni, ciò ke in un sistema reale
    chiamiamo buffer overflow.

    L'unico problema di NS3 è il fatto che alcune classi predefinite come la
    Point-to-Point-Device, aggiungono, ad ogni pacchetto inviato, 2 byte
    extra per identificare il protocollo usato (ipv4, ipv6), essi sono necessari
    per simulazioni con a disposizione lo stack TCP-IP.

    Nel test effettuato ci limitiamo ad un livello
    vicino al canale fisico, quindi per evitare che l'Header riduca la banda
    effettiva per i dati e, per avvicinarsi alla simulazione SCNSL, che non
    prevede l'aggiunta di HEADER, è stata aumentata la banda a 3 Mbps
    in modo che i dati abbiano a disposizione 1 Mbps.

    \pagebreak

    \section{IEEE 802.15 Enviroment Monitoring System}
    L'obbettivo di questo schema è di simulare una tipica situazione di
    monitoraggio ambientale con sensori che comunicano attraverso il protocollo
    IEEE 802.15.4.
    In particolare si fa riferimento ad una situazione dove:

    \begin{itemize}
        \item Un collettore fermo in posizione centrale.
        \item 30 Sensori in movimento attorno al centrale
        \item Ogni sensore ha velocità lineare pari a 1 m/s.
        \item Ogni sensore è distante da 1..30 metri dal collettore.
        \item Pacchetti di dimensione 1 byte.
        \item Ogni sensore invia un pacchetto ogni 1..30 sec.
        \item Nessuna perdita di informazioni dovuta alla distanza.
        \item Canale condiviso con protocollo CSMA/CA.
        \item Tempo simulato 90 secondi.
    \end{itemize}

    \begin{figure}[!ht]
        \centering
        \includegraphics[width=0.75\textwidth]{imgs/enviroment_monitoring.jpg}
        \caption{Schema Enviroment Monitoring System}
    \end{figure}

    \pagebreak

    \subsection{Risultati}

    I seguenti risultati riportano le differenze nel tempo medio e
    tempo massimo che impiega un pacchetto per essere inviato ed
    il corrispondente acknowledge restituito.

    \begin{figure}[!ht]
        \centering
        \includegraphics[width=0.8\textwidth]{imgs/ns3_monitoring}
        \caption{NS3 Monitoring}
    \end{figure}

    \begin{figure}[!ht]
        \centering
        \includegraphics[width=0.8\textwidth]{imgs/scnsl_monitoring}
        \caption{SCNSL Monitoring}
    \end{figure}

    Come si può vedere dai grafici proposti, entrambi i sistemi si equiparano
    per quando riguarda il tempo medio di invio di un pacchetto, invece, per
    quanto riguarda il ritardo massimo SCNSL è di molto superiore.
    Ciò non influisce sulla qualità della simulazione, corretta
    usando entrambi i sistemi

    \pagebreak

    \begin{center}
        \begin{tabular}{l|l}
            Tempo simulazione & 2,496 sec \\
            Tempo simulato & 90,000 sec \\
            \multicolumn{2}{c}{} \\
            \multicolumn{2}{c}{\textit{SCNSL}} \\
        \end{tabular}
        \begin{tabular}{l|l}
            Tempo simulazione & 1,800 sec \\
            Tempo simulato & 90,000 sec \\
            \multicolumn{2}{c}{} \\
            \multicolumn{2}{c}{\textit{NS3}} \\
        \end{tabular}
    \end{center}

    Con questo schema di monitoraggio, SCNSL, si comporta egregiamente
    riusciendo a simulare quasi completamente il funzionamento di una rete
    di sensori che comunicano con il protocollo 802.15.4.
    Il problema principale sta nell'uso della b\_transport o, comunque, della send
    bloccante, perchè non si riesce a simulare l'invio di un pacchetto ad
    un determinato istante di tempo, come potrebbe essere in un ambiente reale.

    NS3, al contrario, permette l'avvio di metodi a determinati istanti
    consentendo di inviare un pacchetto precisamente nel secondo deciso,
    ovviamente, se le condizioni della rete lo permettono ( non ci sono altri
    nodi che tentino di inviare pacchetti ).

    La maggior differenza tra SCNSL e NS3, ciò che purtroppo, fa
    scegliere il secondo simulatore ad un programmatore interessato al
    protocollo 802.15.4 , è la restituzione della tipologia dell'errore durante
    l'invio dei pacchetti, in una modalità veramente developer friendly.

\begin{verbatim}
\\SISTEMA DEI CALLBACK NS3
cb = MakeCallback(&ConfirmCallback::DataConfirm,cc);
device->GetMac()->SetMcpsDataConfirmCallback(cb);

void DataConfirm(McpsDataConfirmParams params){
    switch(params.m_status){
        case SUCCESS...
    }
}\end{verbatim}

    Come si può vedere dal codice qui sopra, NS3 supporta l'uso di un sistema
    di callback che consente di capire il tipo di errore che si è verificato
    dal lato della trasmissione, infatti semplicemente aggiungendo al device
    un callback si possono avere informazioni riguardo il successo o il
    fallimento, compreso il motivo del fallimento, durante l'invio di un
    pacchetto.

    Nota amara per SCNSL è il tempo di simulazione, NS3, pur essendo un
    simulatore molto ampio dal punto di vista delle classi e delle impostazioni
    selezionabili, impiega un secondo in meno rispetto ad SCNSL.


\end{document}
