#include <fstream>
#include <iomanip>
#include <ns3/packet.h>
#include <ns3/lr-wpan-phy.h>
#include <ns3/lr-wpan-mac.h>
#include <ns3/lr-wpan-helper.h>
#include <ns3/lr-wpan-net-device.h>
#include <ns3/simulator.h>
#include <map>
#include <ns3/single-model-spectrum-channel.h>
#include <ns3/constant-position-mobility-model.h>

using namespace ns3;

float omega[31];
float theta[31];

std::map<Mac16Address,int64_t> sendTime;

void MoveDevices(NetDeviceContainer devices){

    for(int i = 1; i < 31; i++){
        theta[i] += omega[i] * 1e-2;

        Ptr<LrWpanNetDevice> device = DynamicCast<LrWpanNetDevice> (devices.Get(i));
        if(device)
            device->GetPhy()->GetMobility()->SetPosition(Vector(cosf(theta[i])*(i),sinf(theta[i])*(i),0));
    }

    Simulator::Schedule(MilliSeconds(10),&MoveDevices,devices);
}

void SendPacket(Ptr<LrWpanNetDevice> device,int delay,Mac16Address destination){

    Ptr<Packet> p0 = Create<Packet> (1);  // 1 byte

    McpsDataRequestParams params;
    params.m_srcAddrMode = SHORT_ADDR;
    params.m_dstAddrMode = SHORT_ADDR;
    params.m_dstPanId = 0;
    params.m_dstAddr = destination;
    params.m_msduHandle = 0;
    params.m_txOptions = TX_OPTION_ACK;

    Simulator::ScheduleNow (&LrWpanMac::McpsDataRequest,device->GetMac(), params, p0);

    sendTime[device->GetMac()->GetShortAddress()] = Simulator::Now().GetMicroSeconds();

    Simulator::Schedule(Seconds(delay),&SendPacket,device,delay,destination);

}

class ConfirmCallback{

public:

    ConfirmCallback(){

    }

    ConfirmCallback(Ptr<LrWpanNetDevice> device){
        this->device = device;
    }

    void DataConfirm(McpsDataConfirmParams params){

        Vector v = this->device->GetPhy()->GetMobility()->GetPosition();
        int64_t old = sendTime[device->GetMac()->GetShortAddress()];
        int64_t now = Simulator::Now().GetMicroSeconds();

        std::string status;
        switch(params.m_status){
        case 0:
            status = "SUCCESS";
            break;
        case 3:
            status = "NO ACC";
            break;
        case 6:
            status = "NO ACK";
            break;
        default:
            status = "ERROR "+ params.m_status;
        }

        std::cout << device->GetMac()->GetShortAddress() << "\t" << status << "\t" << (old/1000000) << "\t" << now-old << "\t" << v.x << "\t" << v.y << std::endl;
    }

private:
    Ptr<LrWpanNetDevice> device;
};

int main (int argc, char *argv[])
{

    std::cout << "Source\tStatus\tSend\tDelay\tX\tY" << std::endl;

    NodeContainer nodes;
    nodes.Create (31);

    LrWpanHelper wpanHelper;

    NetDeviceContainer devices;
    devices = wpanHelper.Install(nodes);
    wpanHelper.AssociateToPan(devices,0);

    Ptr<LrWpanNetDevice> destination = DynamicCast<LrWpanNetDevice> (devices.Get(0));

    for(int i = 1; i < 31; i++){
        Ptr<LrWpanNetDevice> device = DynamicCast<LrWpanNetDevice> (devices.Get(i));
        if(device){
            wpanHelper.AddMobility(device->GetPhy(),CreateObject<ConstantPositionMobilityModel>());
            omega[i] = 1.0f / (i);
            theta[i] = 0;

            //device->GetMac()->SetMacMaxFrameRetries(255); // Aumenta di 2 nodi che possono trasmettere

            ConfirmCallback* cc = new ConfirmCallback(device);
            McpsDataConfirmCallback cb;
            cb = MakeCallback(&ConfirmCallback::DataConfirm,cc);
            device->GetMac ()->SetMcpsDataConfirmCallback (cb);

            Simulator::ScheduleNow(&SendPacket,device,i,destination->GetMac()->GetShortAddress());
        }


    }

    Simulator::ScheduleNow(&MoveDevices,devices);


    Simulator::Stop (Seconds (90));
    Simulator::Run ();
    Simulator::Destroy ();

    return 0;
}
