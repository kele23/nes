#include <fstream>
#include <iomanip>
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"

using namespace ns3;

int sPackets = 0;
int rPackets = 0;
int maxPackets = 2000000;
uint64_t timeout = 1e12;
int currBits = 0;
int currRec  = 0;
int speed = 8;

void SendPacket(Ptr<NetDevice> device);

// STAMPA I RISULTATI
void Print(){
    std::cout   << Simulator::Now().GetPicoSeconds() << "\t"
                << speed << "\t"
                << currBits << "\t"
                << std::setw(8) << std::setfill('0') << currRec << std::endl;
}

//SCHEDULA IL PROSSIMO INVIO DI UN PACCHETTO
void ScheduleTx(Ptr<NetDevice> device){
    static Time old = Simulator::Now();

    Time now = Simulator::Now();

    if( (now - old ) >= Seconds(1) ){

        Print();

        if( speed < (1 << 21)){
            timeout = timeout >> 1;
            speed = speed << 1;
        }
        old = now;
        currBits = 0;
        currRec = 0;
    }

    Simulator::ScheduleNow(&SendPacket,device);
}

//INVIA UN PACCHETTO E METTE IN ATTESA PER IL PROSSIMO INVIO
void SendPacket(Ptr<NetDevice> device){

    Ptr<Packet> p = Create<Packet> (1);
    device->Send (p, device->GetBroadcast (), 0x800);

    sPackets++;
    currBits+=8;
    if(sPackets < maxPackets){
        Simulator::Schedule(PicoSeconds(timeout),&ScheduleTx,device);
    }else{
        Print();
    }

}

//CALLBACK ALLA RICEZIONE DI UN PACCHETTO
bool ReceivedPacket(Ptr<NetDevice> device,Ptr<const Packet> packet,uint16_t t,const Address& address){
    rPackets++;
    currRec+=8;
    return true;
}


int main (int argc, char *argv[])
{

    NodeContainer nodes;
    nodes.Create (2);

    PointToPointHelper pointToPoint;
    pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("3Mbps"));
    pointToPoint.SetChannelAttribute ("Delay", StringValue ("1us"));

    NetDeviceContainer devices;
    devices = pointToPoint.Install (nodes);

    devices.Get(1)->SetReceiveCallback( MakeCallback(&ReceivedPacket) );

    std::cout   << "Time" << "\t\t"
                << "IDeal" << "\t"
                << "Current" << "\t"
                << "Received" << std::endl;

    Simulator::ScheduleNow(&ScheduleTx, devices.Get(0));
    //devices.Get (1)->TraceConnectWithoutContext ("PhyRxDrop", MakeCallback (&RxDrop));

    Simulator::Stop (Seconds (40));
    Simulator::Run ();
    Simulator::Destroy ();

    return 0;
}
